package com.example.tutorialapp_example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class MainActivity extends AppCompatActivity {

    private String SHOWID = "tutorial";

//    @BindView(R.id.btn1)
//    Button btn1;
////    @BindView(R.id.btn2)
////    Button btn2;
////    @BindView(R.id.btn3)
////    Button btn3;
    private Button btn1, btn2, btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ButterKnife.bind(this);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);

        showTutorial();
    }

    private void showTutorial(){
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); //Tiempo entre una transicion y otra (milisegundos)

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWID);
        sequence.setConfig(config);
        sequence.addSequenceItem(btn1, "Este es el boton malo"," Siguiente");
        sequence.addSequenceItem(btn2, "Este es el boton bueno"," Siguiente");
        sequence.addSequenceItem(btn3, "Este es el boton feo","Siguiente");
        sequence.start();
    }
}
